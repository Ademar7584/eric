import { Component, OnInit } from '@angular/core';
import { ListService } from '../services/lista.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css'],
  // providers: [ListService]
})
export class HomeComponent implements OnInit {

  nombre: any;
  listas: any;

  constructor(
    private listService: ListService
  ) {
     }

  ngOnInit() {
    this.lista();
 }

lista() {
  this.listService.list().subscribe((data: any) => {
    this.listas = data;
    const ok = [{
      h: 'dsad',
      q: 2
    }, {
      er: '23'
    }];
    localStorage.setItem('lista', JSON.stringify(ok));
  });
}

viewLocal(){
  let local: any = localStorage.getItem('lista');
  console.log(JSON.parse(local));
}

clearLocal(){
    localStorage.removeItem('lista');
}

}

import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-pagina',
  templateUrl: './pagina.component.html',
  styleUrls: ['./pagina.component.css']
})
export class PaginaComponent implements OnInit {

  constructor(
    private route: ActivatedRoute,
  ) {
    this.route.params.subscribe((val: any) => {
      console.log(val);
    });
   }

  ngOnInit() {
  }

}

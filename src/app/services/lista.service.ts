import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable()

export class ListService {
  
    constructor(private http: HttpClient) {
    }

    list() {
        return this.http.get('https://jsonplaceholder.typicode.com/posts').pipe();
    }

    create(body: any) {
        return this.http.post('https://jsonplaceholder.tyode.com/posts', body).pipe();
    }
}
import { Component, OnInit } from "@angular/core";
import { FormBuilder, Validators } from "@angular/forms";
import { ListService } from "src/app/services/lista.service";

@Component({
    selector: 'app-books-create',
    templateUrl: './create.component.html',
  })
  export class CreateComponent implements OnInit {

    formulario: any = {};
      
    constructor(
      private formBuilder: FormBuilder,
      private listService: ListService
    ) {}

      ngOnInit() {
        this.createForm();
      }

      createForm() {
        this.formulario = this.formBuilder.group({
          titulo: ['', [Validators.required, Validators.minLength(2), Validators.maxLength(10)]],
          autor: [''],
          descripcion: ['']
        });
      }

      post() {
        console.log(this.formulario);
        this.listService.create(this.formulario.value).subscribe(data => {
          console.log(data);
        });
      }

      get titulo() {
        return this.formulario.get('titulo');
      }

      get autor() {
        return this.formulario.get('autor');
      }

      get descripcion() {
        return this.formulario.get('descripcion');
      }

  }
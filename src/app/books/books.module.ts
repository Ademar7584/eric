import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { RouterModule } from "@angular/router";
import { BooksRoutingModule, routedComponents } from "./books.routing";

@NgModule({
    declarations: [...routedComponents],
    imports: [
        RouterModule,
        BooksRoutingModule,
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
    ],
    exports: [
        ...routedComponents
    ]
})
export class BooksModule { }
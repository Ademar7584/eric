import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { BooksComponent } from "./books.component";
import { CreateComponent } from "./create/create.component";
import { ListComponent } from "./list/list.component";

export const routes: Routes = [
    {
        path: '',
        component: BooksComponent,
        children: [
            {
                path: 'list',
                component: ListComponent
            },
            {
                path: 'create',
                component: CreateComponent
            }
        ]
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})

export class BooksRoutingModule {}
export const routedComponents = [
    BooksComponent,
    ListComponent,
    CreateComponent
]
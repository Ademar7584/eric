import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { BrowserModule } from "@angular/platform-browser";
import { RouterModule, Routes } from "@angular/router";
import { ListComponent } from "./books/list/list.component";
import { HomeComponent } from "./home/home.component";
import { PadreComponent } from "./padre/padre.component";

const routes: Routes = [
    {
        path: "",
        component: HomeComponent,
        pathMatch: 'full'
    }, {
        path: 'list',
        component: ListComponent
    }, { 
        path: 'book',
        loadChildren: () => import('../app/books/books.module').then(x => x.BooksModule)
     }, {
       path: 'padre',
       component: PadreComponent
     }
];

@NgModule({
    imports: [
      CommonModule,
      BrowserModule,
      RouterModule.forRoot(routes, {
        useHash: true,
        scrollPositionRestoration: 'enabled'
      })],
    exports: [RouterModule]
  })
  export class AppRoutingModule { }
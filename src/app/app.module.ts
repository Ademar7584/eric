import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { PaginaComponent } from './pagina/pagina.component';
import { ListaComponent } from './lista/lista.component';
import { RouterModule } from '@angular/router';
import { ListService } from './services/lista.service';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { CommonModule } from '@angular/common';
import { HomeComponent } from './home/home.component';
import { AppRoutingModule } from './app-routing.module';
import { PadreComponent } from './padre/padre.component';
import { HijoComponent } from './hijo/hijo.component';
import { InterceptorService } from './interceptors/interceptors.service';

@NgModule({
  declarations: [
    AppComponent,
    PaginaComponent,
    ListaComponent,
    HomeComponent,
    PadreComponent,
    HijoComponent
  ],
  imports: [
    BrowserAnimationsModule,
    BrowserModule,
    RouterModule,
    HttpClientModule,
    CommonModule,
    AppRoutingModule
  ],
  providers: [
    ListService,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: InterceptorService,
      multi: true
    }
  ],
  
  bootstrap: [AppComponent]
})
export class AppModule { }

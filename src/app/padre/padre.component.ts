import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-padre',
  templateUrl: './padre.component.html',
  styleUrls: ['./padre.component.css']
})
export class PadreComponent implements OnInit {
  
  items = ['item1', 'item2', 'item3', 'item4'];

  constructor() { }

  ngOnInit(): void {
  }

  addItem(newItem: string) {
    console.log(newItem);
    this.items.unshift(newItem);
  }

}
